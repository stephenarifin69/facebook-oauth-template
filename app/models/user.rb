class User < ActiveRecord::Base

  devise :database_authenticatable, :omniauthable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :authentications, dependent: :delete_all

  def index

  end

  def apply_omniauth(omniauth)

    self.name = omniauth['info']['nickname'] if name.blank?
    self.email = omniauth['info']['email'] if email.blank?
    self.image = omniauth['info']['image'] if image.blank?

    authentications.build(provider: omniauth['provider'],
                          uid: omniauth['uid'],
                          token: omniauth['credentials']['token'])
  end

  def password_required?
    (authentications.empty? || !password.blank?) && super
  end

  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end

end
